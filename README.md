# SelectWithSearch
SelectWithSearch intends to add search option on select field.

 - For a full description of the module, visit the project page:
   https://www.drupal.org/project/selectwithsearch

 - To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/search/selectwithsearch 

## CONTENTS OF THIS FILE

 - Features
 - Requirements
 - Credit
 - Installation
 - Configuration
 - Maintainers
 
   
## FEATURES   

- Module will add new widget called "Select With Search".

## REQUIREMENTS

- No special requirements.

## CREDIT

- Module is using downloaded version of js and css of "Select2 4.0.1" Jquery Library.  
 

## INSTALLATION

 - Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


## CONFIGURATION

 - Configure the SelectWithSearch at (/admin/structure/types/manage/<content type>/form-display).   
 
## MAINTAINERS

