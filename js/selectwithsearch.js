/*
@File is designed to add searchable option for select field
*/
(function ($, Drupal) {
  Drupal.behaviors.advancedSelect = {
    attach: function attach(context, settings) {
        $(".custom_select").select2( {
			 allowClear: true
		});
    }
  };

})(jQuery, Drupal);
