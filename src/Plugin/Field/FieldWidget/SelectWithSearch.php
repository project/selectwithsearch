<?php

namespace Drupal\selectwithsearch\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsWidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'selectwithsearch' widget.
 *
 * @FieldWidget(
 *   id = "selectwithsearch",
 *   module = "selectwithsearch",
 *   label = @Translation("Select With Search"),
 *   field_types = {
 *     "list_string",
 *	   "list_integer",
 *     "list_float",
 *     "entity_reference", 
 *   }
 * )
 */
class SelectWithSearch extends OptionsWidgetBase {

  /**
   * {@inheritdoc}
   */

  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['value'] = $element + [
      '#type' => 'select',
      '#title' => $this->fieldDefinition->getLabel(),
      '#description' => $this->fieldDefinition->getDescription(),
      '#default_value' => $items[$delta]->value ?? NULL,
      '#options' => $this->getOptions($items->getEntity()),
      '#attributes' => [
        'class' => ['custom_select'],
      ],
	  '#attached' => [
        'library' => [
          'selectwithsearch/selectwithsearch'
        ],
      ],
    ];


    return $element;

  }


}
